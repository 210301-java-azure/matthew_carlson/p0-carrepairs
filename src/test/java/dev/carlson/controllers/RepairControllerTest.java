package dev.carlson.controllers;

import dev.carlson.models.*;
import dev.carlson.services.RepairService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class RepairControllerTest {
    /*
    - Mocking ctx.header() has the same inconsistency issue as ctx.pathParam(), so any tests for handleGetAllUserRepairs suffer
    - I also can't stub the static AuthController method, so they have to be tested w/ integration testing
        Expect 4/8 passing because of this
     */

    @InjectMocks
    private RepairController repairController;

    @Mock
    private RepairService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testHandleGetAllRepairs(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair());
        repairs.add(new Repair());
        repairs.add(new Repair());

        when(service.getAllRepairs()).thenReturn(repairs);
        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllRepairsStatus(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair());

        when(service.getAllRepairs("PENDING", null)).thenReturn(repairs);
        when(context.queryParam("status")).thenReturn("PENDING");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllRepairsProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        repairs.add(new Repair());
        repairs.add(new Repair());

        when(service.getAllRepairs(null, "bad")).thenReturn(repairs);
        when(context.queryParam("problem")).thenReturn("bad");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    void testHandleGetAllRepairsStatusProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        Account account = new Account(1);
        repairs.add(new Repair(account, "bad", RepairStatus.PENDING));

        when(service.getAllRepairs("PENDING", "bad")).thenReturn(repairs);
        when(context.queryParam("status")).thenReturn("PENDING");
        when(context.queryParam("problem")).thenReturn("bad");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    @Disabled
    void testHandleGetAllUserRepairs(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        Account account = new Account(1);
        repairs.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));
        repairs.add(new Repair(account, "", RepairStatus.PENDING));
        repairs.add(new Repair(account, "", RepairStatus.COMPLETED));

        when(service.getRepairsForUser(account)).thenReturn(repairs);
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);
        when(context.header("user-id")).thenReturn("1");

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    @Disabled
    void testHandleGetAllUserRepairsStatus(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        Account account = new Account(1);
        repairs.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));

        when(service.getRepairsForUser(account, "IN_PROGRESS", null)).thenReturn(repairs);
        when(context.queryParam("status")).thenReturn("IN_PROGRESS");
        // context.header() has the same problem as ctx.pathParam(), so tests will be inconsistent
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    @Disabled
    void testHandleGetAllUserRepairsProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        Account account = new Account(1);
        repairs.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));
        repairs.add(new Repair(account, "", RepairStatus.PENDING));
        repairs.add(new Repair(account, "", RepairStatus.COMPLETED));

        when(service.getRepairsForUser(account, null, "bad")).thenReturn(repairs);
        when(context.header("user-id")).thenReturn("1");
        when(context.queryParam("problem")).thenReturn("bad");
        // context.header() has the same problem as ctx.pathParam(), so tests will be inconsistent
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    @Disabled
    void testHandleGetAllUserRepairsStatusProblem(){
        Context context = mock(Context.class);

        List<Repair> repairs = new ArrayList<>();
        Account account = new Account(1);
        repairs.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));
        repairs.add(new Repair(account, "", RepairStatus.PENDING));
        repairs.add(new Repair(account, "", RepairStatus.COMPLETED));

        when(service.getRepairsForUser(account, "PENDING", "bad")).thenReturn(repairs);
        when(context.header("user-id")).thenReturn("1");
        when(context.queryParam("status")).thenReturn("PENDING");
        when(context.queryParam("problem")).thenReturn("bad");
        // context.header() has the same problem as ctx.pathParam(), so tests will be inconsistent
        when(context.header("Authorization")).thenReturn("token");
        AuthInfo info = new AuthInfo();
        info.setRole(AccountRole.ADMIN);
        // So it turns out it's hard/impossible to mock static methods.
        when(AuthController.getUserInfoFromToken("token")).thenReturn(info);

        repairController.handleGetAllRepairs(context);
        verify(context).json(repairs);
    }

    @Test
    @Disabled
    void testHandleCreateRepair(){

    }

    @Test
    @Disabled
    void testHandleUpdateRepair(){

    }
}
