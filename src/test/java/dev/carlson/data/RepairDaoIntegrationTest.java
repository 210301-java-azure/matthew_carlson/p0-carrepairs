package dev.carlson.data;

import dev.carlson.JavalinApp;
import dev.carlson.models.Repair;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RepairDaoIntegrationTest {

	/*
	For some reason, asObject(Repair.class) or as a list of repairs just fails, and the response body is null.
	The response is fine, and there, but the mapping just doesn't work. Placing them as Object references (or anything
	but Repair objects as far as I can tell) means that I can at least test that SOMETHING is there, which is all I'm
	testing for anyway. I'd like to treat them like Repair objects but I can't figure out what's happening.
	 */
	private static JavalinApp app = new JavalinApp();
	private static String adminAuthToken;
	private static String user4AuthToken;

	@BeforeAll
	public static void startService(){
		app.start(7070);
		HttpResponse<String> adminResponse = Unirest.post("http://localhost:7070/login")
				.field("email", "admin@admin")
				.field("password", "password")
				.asString();
		adminAuthToken = adminResponse.getHeaders().get("Authorization").get(0);
		HttpResponse<String> userResponse = Unirest.post("http://localhost:7070/login")
				.field("email", "ytunnow3@rediff.com")
				.field("password", "Reduced")
				.asString();
		user4AuthToken = userResponse.getHeaders().get("Authorization").get(0);
	}

	@AfterAll
	public static void stopService(){
		app.stop();
	}

	@Test
	void testGetAllRepairs(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/all-repairs")
				.header("Authorization", adminAuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetAllRepairsStatus(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/all-repairs")
				.queryString("status", "PENDING")
				.header("Authorization", adminAuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetAllRepairsProblem(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/all-repairs")
				.queryString("problem", "et")
				.header("Authorization", adminAuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetAllRepairsStatusProblem(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/all-repairs")
				.queryString("status", "PENDING")
				.queryString("problem", "et")
				.header("Authorization", adminAuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetRepairsForUser(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/repairs")
				.header("Authorization", user4AuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetRepairsForUserStatus(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/repairs")
				.queryString("status", "IN_PROGRESS")
				.header("Authorization", user4AuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetRepairsForUserProblem(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/repairs")
				.queryString("problem", "et")
				.header("Authorization", user4AuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetRepairsForUserStatusProblem(){
		HttpResponse<List<Object>> response = Unirest.get("http://localhost:7070/repairs")
				.queryString("status", "IN_PROGRESS")
				.queryString("problem", "et")
				.header("Authorization", user4AuthToken)
				.asObject(new GenericType<List<Object>>() {});
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody().size()>0)
		);
	}

	@Test
	void testGetRepairById(){
		HttpResponse<Object> response = Unirest.get("http://localhost:7070/all-repairs/1")
				.header("Authorization", adminAuthToken)
				.asObject(Object.class);
		assertAll(
				()->assertEquals(200, response.getStatus()),
				()->assertTrue(response.getBody()!=null)
		);
	}

	// How to automate testing of add/update/delete when the id will be different every run?
}
