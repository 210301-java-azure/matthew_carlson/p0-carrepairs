package dev.carlson.services;

import dev.carlson.data.RepairDAO;
import dev.carlson.models.Account;
import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import io.javalin.http.BadRequestResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RepairServiceTest {

    @InjectMocks
    private RepairService repairService;

    @Mock
    private RepairDAO repairDAO;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllRepairsBadStatus(){
        assertThrows(BadRequestResponse.class,
                ()->repairService.getAllRepairs("bad", null),
                "status query only applicable for 'PENDING', 'IN_PROGRESS', or 'COMPLETED'");
    }

    @Test
    void testGetAllRepairsStatus(){
        List<Repair> list = new ArrayList<>();
        Account account = new Account(1);
        list.add(new Repair(account, "problem string", RepairStatus.PENDING));

        when(repairDAO.getAllRepairs(RepairStatus.PENDING)).thenReturn(list);
        assertEquals(list, repairService.getAllRepairs("PENDING", null));
    }

    @Test
    void testGetAllRepairsProblem(){
        List<Repair> list = new ArrayList<>();
        Account account = new Account(1);
        list.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));

        when(repairDAO.getAllRepairs("problem")).thenReturn(list);
        assertEquals(list, repairService.getAllRepairs(null, "problem"));
    }

    @Test
    void testGetAllRepairsStatusAndProblem(){
        List<Repair> list = new ArrayList<>();
        Account account = new Account(1);
        list.add(new Repair(account, "problem string", RepairStatus.PENDING));

        when(repairDAO.getAllRepairs(RepairStatus.PENDING, "problem")).thenReturn(list);
        assertEquals(list, repairService.getAllRepairs("PENDING", "problem"));
    }

    @Test
    void testGetRepairsForUserBadStatus(){
        assertThrows(BadRequestResponse.class,
                ()->repairService.getRepairsForUser(new Account(), "bad", null),
                "status query only applicable for 'PENDING', 'IN_PROGRESS', or 'COMPLETED'");
    }

    @Test
    void testGetRepairsForUserStatus(){
        List<Repair> list = new ArrayList<>();
        Account account = new Account(1);
        list.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));

        when(repairDAO.getRepairsForUser(account, RepairStatus.PENDING)).thenReturn(list);
        assertEquals(list, repairService.getRepairsForUser(account, "PENDING", null));
    }

    @Test
    void testGetRepairsForUserProblem(){
        List<Repair> list = new ArrayList<>();
        Account account = new Account(1);
        list.add(new Repair(account, "problem string", RepairStatus.IN_PROGRESS));

        when(repairDAO.getRepairsForUser(account, "problem")).thenReturn(list);
        assertEquals(list, repairService.getRepairsForUser(account, null, "problem"));
    }

    @Test
    void testGetRepairsForUserStatusAndProblem(){
        List<Repair> list = new ArrayList<>();
        Account account = new Account(1);
        list.add(new Repair(account, "problem string", RepairStatus.PENDING));

        when(repairDAO.getRepairsForUser(account, RepairStatus.PENDING, "problem")).thenReturn(list);
        assertEquals(list, repairService.getRepairsForUser(account, "PENDING", "problem"));
    }

}
