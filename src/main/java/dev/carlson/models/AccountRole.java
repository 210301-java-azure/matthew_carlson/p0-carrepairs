package dev.carlson.models;

public enum AccountRole {
    ADMIN, CUSTOMER
}
