package dev.carlson.models;


import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Repair implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Account account;
    private String make;
    private String model;
    private int year;
    @Column(name = "date_submitted")
    private Timestamp dateSubmitted;
    @Column(name = "date_completed")
    private Timestamp dateCompleted;
    private double price;
    @Column(columnDefinition = "VARCHAR(5000)")
    private String problem;
    @Column(columnDefinition = "VARCHAR(5000)")
    private String solution;
    private RepairStatus status;

    public Repair(){}

    public Repair(int id){
        this.id = id;
    }

    public Repair(Account account, String problem, RepairStatus status){
        this.account = account;
        this.problem = problem;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Timestamp getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Timestamp dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public Timestamp getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(Timestamp dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Repair repair = (Repair) o;
        return id == repair.id && year == repair.year && Double.compare(repair.price, price) == 0 && Objects.equals(account, repair.account) && Objects.equals(make, repair.make) && Objects.equals(model, repair.model) && Objects.equals(dateSubmitted, repair.dateSubmitted) && Objects.equals(dateCompleted, repair.dateCompleted) && Objects.equals(problem, repair.problem) && Objects.equals(solution, repair.solution) && status == repair.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, make, model, year, dateSubmitted, dateCompleted, price, problem, solution, status);
    }

    @Override
    public String toString() {
        return "Repair{" +
                "id=" + id +
                ", account=" + account +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", dateSubmitted=" + dateSubmitted +
                ", dateCompleted=" + dateCompleted +
                ", price=" + price +
                ", problem='" + problem + '\'' +
                ", solution='" + solution + '\'' +
                ", status=" + status +
                '}';
    }
}
