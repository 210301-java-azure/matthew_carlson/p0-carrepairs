package dev.carlson.models;

public enum RepairStatus {
    PENDING, IN_PROGRESS, COMPLETED
}
