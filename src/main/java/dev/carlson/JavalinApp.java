package dev.carlson;

import dev.carlson.controllers.AccountController;
import dev.carlson.controllers.AuthController;
import dev.carlson.controllers.RepairController;
import dev.carlson.utilities.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
	RepairController repairController = new RepairController();
	AccountController accountController = new AccountController();
	AuthController authController = new AuthController();
	SecurityUtil securityUtil = new SecurityUtil();

	// TODO: swap to enable only the hosted URL
	Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(() -> {
		path("repairs", () -> {
			before("/", authController::authorizeToken);
			get(repairController::handleGetAllUserRepairs);
			path(":id", () -> {
				before("/", authController::authorizeToken);
				get(repairController::handleGetUserRepairById);
			});
		});
		path("all-repairs", () -> {
			before("/", authController::authorizeAdminToken);
			get(repairController::handleGetAllRepairs);
			post(repairController::handleCreateRepair);
			path(":id", () -> {
				before("/", authController::authorizeAdminToken);
				get(repairController::handleGetRepairById);
				delete(repairController::handleDeleteRepair);
				put(repairController::handleUpdateRepair);
			});
		});
		path("accounts", () -> {
			get(accountController::handleGetAllAccounts);
			post(accountController::handlePostAccount);
			path(":id", () -> {
				before("/", authController::authorizeToken);
				get(accountController::handleGetAccountById);
				put(accountController::handleUpdateAccount);
				delete(accountController::handleDeleteAccount);
			});
		});
		path("login", () -> {
			post(authController::authenticateLogin);
			after("/", securityUtil::attachResponseHeaders);
		});
	});

	public void start(int port) {
		this.app.start(port);
	}

	public void stop() {
		this.app.stop();
	}

}
