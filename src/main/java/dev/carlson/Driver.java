package dev.carlson;

public class Driver {
    public static void main(String[] args) {
        /*
        Ideal changes
        - Make the JWT refresh the timeout on requests, rather than a static 3 hour timeout
        - redirect user from admin pages if they aren't logged in as admin
         */
        /*
        Known bugs:
        After the server has been up for a few hours, there are some socket write / db connection errors
        I think it has to do with connections becoming stale from being idle
                "Connection reset by peer: socket write error"
            Stack trace:
                org.hibernate.exception.JDBCConnectionException: Unable to acquire JDBC Connection
                    my dao/service/controller are in the stack here
                Caused by: com.microsoft.sqlserver.jdbc.SQLServerException: Connection reset by peer: socket write error
                Caused by: java.net.SocketException: Connection reset by peer: socket write error
         */
        /*
        TODO:
        - documentation
        - password hashing
        - pagination eventually
         */
        JavalinApp javalinApp = new JavalinApp();
        javalinApp.start(80);
    }
}
