package dev.carlson.data;

import dev.carlson.models.Account;

import java.util.List;

public interface AccountDAO {
    public List<Account> getAllAccounts();
    public Account getAccountById(int id);
    public Account getAccountByEmail(String email);

    public Account addAccount(Account account);
    public Account updateAccount(Account account);
    public void deleteAccount(int id);
}
