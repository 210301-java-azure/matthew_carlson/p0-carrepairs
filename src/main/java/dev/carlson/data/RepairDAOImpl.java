package dev.carlson.data;

import dev.carlson.models.Account;
import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;
import dev.carlson.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class RepairDAOImpl implements RepairDAO{
	@Override
	public List<Repair> getAllRepairs() {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair order by dateSubmitted desc", Repair.class).list();
		}
	}

	@Override
	public List<Repair> getAllRepairs(RepairStatus status) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.status = :status order by dateSubmitted desc", Repair.class)
					.setParameter("status", status)
					.list();
		}
	}

	@Override
	public List<Repair> getAllRepairs(String problem) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.problem like :problem order by dateSubmitted desc", Repair.class)
					.setParameter("problem", '%' + problem + '%')
					.list();
		}
	}

	@Override
	public List<Repair> getAllRepairs(RepairStatus status, String problem) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.status = :status and r.problem like :problem order by dateSubmitted desc", Repair.class)
					.setParameter("status", status)
					.setParameter("problem", '%' + problem + '%')
					.list();
		}
	}

	@Override
	public List<Repair> getRepairsForUser(Account account) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.account = :account order by dateSubmitted desc", Repair.class)
					.setParameter("account", account)
					.list();
		}
	}

	@Override
	public List<Repair> getRepairsForUser(Account account, RepairStatus status) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.account = :account and status = :status order by dateSubmitted desc", Repair.class)
					.setParameter("account", account)
					.setParameter("status", status)
					.list();
		}
	}

	@Override
	public List<Repair> getRepairsForUser(Account account, String problem) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.account = :account and problem like :problem order by dateSubmitted desc", Repair.class)
					.setParameter("account", account)
					.setParameter("problem", '%' + problem + '%')
					.list();
		}
	}

	@Override
	public List<Repair> getRepairsForUser(Account account, RepairStatus status, String problem) {
		try(Session session = HibernateUtil.getSession();){
			return session.createQuery("from Repair r where r.account = :account and status = :status and problem like :problem order by dateSubmitted desc", Repair.class)
					.setParameter("account", account)
					.setParameter("status", status)
					.setParameter("problem", '%' + problem + '%')
					.list();
		}
	}

	@Override
	public Repair getRepairById(int id) {
		try (Session session = HibernateUtil.getSession();){
			return session.get(Repair.class, id);
		}
	}

	@Override
	public Repair addRepair(Repair repair) {
		try (Session session = HibernateUtil.getSession();){
			Transaction tx = session.beginTransaction();
			int id = (int) session.save(repair);
			repair.setId(id);
			tx.commit();
			return repair;
		}
	}

	@Override
	public Repair updateRepair(Repair repair) {
		try (Session session = HibernateUtil.getSession();){
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(repair);
			tx.commit();
			return repair;
		}
	}

	@Override
	public void deleteRepair(int id) {
		try (Session session = HibernateUtil.getSession();){
			Transaction tx = session.beginTransaction();
			Repair repair = new Repair(id);
			session.delete(repair);
			tx.commit();
		}
	}
}
