package dev.carlson.data;

import dev.carlson.models.Account;
import dev.carlson.models.Repair;
import dev.carlson.models.RepairStatus;

import java.util.List;

public interface RepairDAO {
    public List<Repair> getAllRepairs();
    public List<Repair> getAllRepairs(RepairStatus status);
    public List<Repair> getAllRepairs(String problem);
    public List<Repair> getAllRepairs(RepairStatus status, String problem);
    public List<Repair> getRepairsForUser(Account account);
    public List<Repair> getRepairsForUser(Account account, RepairStatus status);
    public List<Repair> getRepairsForUser(Account account, String problem);
    public List<Repair> getRepairsForUser(Account account, RepairStatus status, String problem);
    public Repair getRepairById(int id);

    public Repair addRepair(Repair repair);
    public Repair updateRepair(Repair repair);
    public void deleteRepair(int id);
}
