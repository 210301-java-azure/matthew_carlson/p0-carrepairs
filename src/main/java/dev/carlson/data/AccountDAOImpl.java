package dev.carlson.data;

import dev.carlson.models.Account;
import dev.carlson.models.Repair;
import dev.carlson.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class AccountDAOImpl implements AccountDAO{
	@Override
	public List<Account> getAllAccounts() {
		try(Session session = HibernateUtil.getSession()){
			return session.createQuery("from Account", Account.class).list();
		}
	}

	@Override
	public Account getAccountById(int id) {
		try(Session session = HibernateUtil.getSession()){
			return session.get(Account.class, id);
		}
	}

	@Override
	public Account getAccountByEmail(String email) {
		try(Session session = HibernateUtil.getSession()){
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Account> cq = cb.createQuery(Account.class);

			Root<Account> root = cq.from(Account.class);
			cq.select(root);

			cq.where(cb.equal(root.get("email"), email));

			Query<Account> query = session.createQuery(cq);
			return query.getSingleResult();
		}
	}

	@Override
	public Account addAccount(Account account) {
		try(Session session = HibernateUtil.getSession()){
			Transaction tx = session.beginTransaction();
			int id = (int) session.save(account);
			account.setId(id);
			tx.commit();
			return account;
		}
	}

	@Override
	public Account updateAccount(Account account) {
		try(Session session = HibernateUtil.getSession()){
			Transaction tx = session.beginTransaction();
			session.update(account);
			tx.commit();
			return account;
		}
	}

	@Override
	public void deleteAccount(int id) {
		// Everything I read says there's easier ways to do this, but it just wasn't working
		// At least this functions correctly
		try(Session session = HibernateUtil.getSession()){
			EntityManager em = session.getEntityManagerFactory().createEntityManager();
			em.getTransaction().begin();
			// get and delete any repairs the user has
			Account deletedAccount = new Account(id);
			List<Repair> userRepairList = session.createQuery("from Repair r where r.account = :account order by dateSubmitted desc", Repair.class)
					.setParameter("account", deletedAccount)
					.list();
			for (Repair repair : userRepairList) {
				em.remove(repair);
			}
			// delete the account
			em.remove(deletedAccount);
			em.getTransaction().commit();
			em.close();
		}
	}
}
