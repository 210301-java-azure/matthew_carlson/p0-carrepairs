package dev.carlson.utilities;


import io.javalin.http.Context;

public class SecurityUtil {

    public void attachResponseHeaders(Context ctx){
        ctx.header("Access-Control-Expose-Headers", "Authorization, User-Id");
    }
}
