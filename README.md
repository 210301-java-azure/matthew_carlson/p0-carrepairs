# Auto Repair Webservice
This webservice allows customers and mechanics to store information about auto reapirs to let customers view the status of their repairs and their cost, and for mechanics to look back at old repairs to reference a vehicle history and previous solutions.
All users will have an account saved in the database and be able to access only information that is relevant to them.

## Data model
Repair object as json
```javascript
{
    "id": 1,
    "clientId": 2,
    "make": "another",
    "model": "name",
    "year": 2015,
    "dateSubmitted": 1586750400000,
    "dateCompleted": null,
    "price": 50.0,
    "problem": "some problem",
    "solution": "some solution",
    "status": "IN_PROGRESS"
}
```

Account object as json
```javascript
{
    "id": 4,
    "firstName": "Yetti",
    "lastName": "Tunnow",
    "email": "ytunnow3@rediff.com",
    "password": "Reduced",
    "role": "CUSTOMER"
}
```

## Available endpoints and verbs
### endpoints requiring authentication
`/repairs`
`/all-repairs`
`/accounts`
### endpoints that don't require authentication
`/login`
`/create-account`
## repairs
` get /repairs` 
- returns all repairs in the database for the logged in user as a json list. Requires Authentication header.
- Query params available: status and problem
    - status searches by the repair status, and must be either `PENDING`, `IN_PROGRESS`, or `COMPLETED`.
    - problem matches the problem description given to the repair, to easily find repairs for specific problems.

`get /repairs/:id`
- Returns a specific repair in json, identified by an id number. Only returns the repair if it belongs to the logged in user, otherwise returns Unauthorized.

## all-repairs
`get /all-repairs`
- Admin use only, returns all repairs on the database in a json list.

`post /all-repairs`
- Creates new repairs on the database. Only admins can create new repairs. Provide a json object version of a repair object. Id, date submitted and completed are automatically set.

`get /all-repairs/:id`
- Returns a specific repair in json. The path is restricted to admins, so any repair can be returned to them.

`put /all-repairs/:id`
- Update a repair by providing a complete and updated json object of the repair. Only admins can update repairs

`delete /all-repairs/:id`
- Delete a repair object from the database. Only admins can delete repairs.

## accounts
`get /accounts`
- returns all accounts in the database as a json list. Only admins are able to view the list.

`get /accounts/:id`
- returns a single account in json as specified by an id number. Only returns if it is the account of the current user, or if the current user is an admin.

`put /accounts/:id`
- updates an account by providing a json object with updated values. Only allowed if a user is updating their own account or if the user is admin.

`delete /accounts/:id`
- deletes an account referenced by the id in the path. Only allowed if a user is deleting their own account or the user is an admin.

## login
`post /login`
- allows all users to login and obtain a JWT for further use of the webservice. The JWT is returned in a response header `Authorization` and must be placed in a request header `Authorization` for future requests. The JWT has an expiration time for 3 hours after the token was created.

## create-account
`post /create-account`
- Allows all users to create a new account when providing a json object of an account. Users will then have to login to obtain a token and use the service.