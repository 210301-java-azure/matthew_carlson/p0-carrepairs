FROM java:8
COPY build/libs/CarRepairsFrontend-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar CarRepairsFrontend-1.0-SNAPSHOT.jar