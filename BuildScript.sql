-- public.account definition

-- Drop table

-- DROP TABLE public.account;

CREATE TABLE public.account (
	id serial NOT NULL,
	first_name varchar(80) NULL,
	last_name varchar(80) NULL,
	email varchar(150) NULL,
	"password" varchar(80) NULL,
	user_role varchar(50) NULL,
	CONSTRAINT account_pkey PRIMARY KEY (id),
	CONSTRAINT unique_email_const UNIQUE (email)
);

-- public.repair definition

-- Drop table

-- DROP TABLE public.repair;

CREATE TABLE public.repair (
	id serial NOT NULL,
	client_id int4 NULL,
	make varchar(80) NULL,
	model varchar(80) NULL,
	"year" int4 NULL,
	date_submitted timestamp NULL,
	date_completed timestamp NULL,
	price numeric(8,2) NULL,
	problem text NULL,
	solution text NULL,
	status varchar(15) NULL,
	CONSTRAINT repair_pkey PRIMARY KEY (id)
);


-- public.repair foreign keys

ALTER TABLE public.repair ADD CONSTRAINT repair_client_id_fkey FOREIGN KEY (client_id) REFERENCES account(id);